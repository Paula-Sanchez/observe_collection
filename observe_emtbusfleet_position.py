'''
Created on 14 ago. 2018

@author: MobilityLabs Madrid
'''
import time
import datetime
import json
import sys

from MeteorClient import MeteorClient

'''
This code is a example for make a connection from a Reactive Box Mobility Madrid 
The Reactive Box is a Meteor Server which includes many layers of data. 
Current example shows the way of make subscription at value of real time bus positions 
    
For use this program must be include:
MeteorClient https://github.com/hharnisc/python-meteor
EventEmitter https://github.com/jfhbrook/pyee 
Websockets (ws4py on https://github.com/Lawouach/WebSocket-for-Python)
ddpClient https://pypi.python.org/pypi/python-ddp/0.1.0
'''   



def subscribed(subscription):
    print('* SUBSCRIBED {}'.format(subscription))


def unsubscribed(subscription):
    print('* UNSUBSCRIBED {}'.format(subscription))


def added(collection, id, fields):
    #print('* ADDED {} {}'.format(collection, id))
    
    if collection == "users":
        #print collection (if you want to suscribe to everyone)
        #client.subscribe('BUSMADRID.flotapos.all')
        #atocha (test for buses nearby Atocha)
        #client.subscribe("BUSMADRID.flotapos.custom",[{"geometry": {"$near": {"$geometry": {"type": "Point" ,"coordinates": [-3.692045, 40.408812 ]},"$maxDistance": 100,"$minDistance": 0}}}])
        #list of some buses 
        #client.subscribe("BUSMADRID.flotapos.custom",[{"_id":{"$in": ["1018","2018"]}}])
		client.subscribe('BUSMADRID.flotapos.all')

    elif collection == "BUSMADRID.flotapos":
        try:
            print "item suscribed...id: "+str(id)+" values: "+str(fields)
        except:
            
             msgLog =  "Error in parameters %s" % (sys.exc_info()[1])
             print msgLog
  
             
def changed(collection, id, fields, cleared):
    
    #print ("changed:{}".format(id)+" fields: {}".format(fields))
    try:
        if collection == "BUSMADRID.flotapos":
            print "item changed...id: "+str(id)+" values: "+str(fields)
    except:
        
         msgLog =  "Error in parameters %s" % (sys.exc_info()[1])
         print msgLog
    


def connected():
    print('* CONNECTED')
    client.login("your idClient", "your passKey")
 

def subscription_callback(error):
    if error:
        print(error)

try:
    
    client = MeteorClient('wss://rbmobility.emtmadrid.es/websocket',auto_reconnect=True,auto_reconnect_timeout=5,debug=False)

    client.on('subscribed', subscribed)
    client.on('unsubscribed', unsubscribed)
    client.on('added', added)
    client.on('connected', connected)
    client.on('changed',changed)
    
    
    
    client.connect()


    # ctrl + c to kill the script
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break
    
   

except Exception as err :
    print err.message

