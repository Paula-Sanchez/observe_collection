# reactive_samples

MobilityLabs Madrid allows to use a Meteor js portal for getting public and private data (of course, you can put your own data into). 

Remember, you will need a credential passKey for using the data from ReactiveBox MobilityLabs Portal. In order to get your credentials, please, link to https://mobilitylabs.emtmadrid.es han select "Portal de Desarrolladores" zone. Once you are registered, you can use those credentials for observe (Reactive), get (HTTP), upload (plain text) or put (Rabbit MQ) item into collections or datasets.

For questions, please, ask to Portal administrator via email to opendata@emtmadrid.es

Share and Enjoy!!